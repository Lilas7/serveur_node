var app = require('express')();
var bodyParser  =  require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


var http = require('http').createServer(app);
var io = require('socket.io')(http);
var mysql      = require('mysql');

var db = mysql.createConnection({
  database: 'badge',
  host: "localhost",
  user: "user",
  password: "root"
});

db.connect(function(err){
    if (err) console.log(err)
    else console.log('db connected');
})

var logs = [];
var isInitNotes = false;
var socketCount = 0;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/listeaccess.html');
});

app.get('/listAutorisation', function(req, res){
  res.sendFile(__dirname + '/listeaccess.html');
});

app.get('/listLogs', function(req, res){
  res.sendFile(__dirname + '/listelogs.html');
});

app.get('/listeBadges', function(req, res){
  res.sendFile(__dirname + '/listebadges.html');
});

app.post('/pushInfos',function(req,res){

	function checkLastVersion(callback){

		db.query(
			"SELECT * FROM badging ORDER BY ID DESC LIMIT 1", function (err, result, fields) {
		    
		    if (err){
		    	return 
		    }else{
		    	if( result[0].version != null ){
		    		pushRow(result[0].version)
		    	}else{
		    		res.status(200).json({ message: 'error' });
		    	}
			}
		});
	}

	function pushRow(version){

		var v = version+0.0001;

		console.log(version+0.0001);

		db.query(
			"INSERT INTO badging (zone_id, uid_badge, created_at, checking_infos, version, inside) "+
			"VALUES ('"+req.body.zone+"', '"+req.body.idBadge+"', '"+req.body.created_at+"', '"+req.body.checking_info+
		"', '"+v+"' , '"+req.body.inside+"')");

		db.query(
			"SELECT * FROM badging "+
			"LEFT JOIN zones ON badging.zone_id = zones.id "+
			"LEFT JOIN badges ON badging.uid_badge = badges.uid "+
			"ORDER BY badging.id DESC LIMIT 1", function (err, result, fields) {
		    
		    if (err){
		    	res.status(200).json({ message: 'error'});
		    }else{
		    	io.emit('pushData', result );
			}

		});		

		res.status(200).json({ message: 'success' });
				
	}

	checkLastVersion();
	
});


// app.post('/pushSetZone',function(req,res){

// 	function checkLastVersion(callback){

// 		db.query(
// 			"SELECT * FROM acces ORDER BY ID DESC LIMIT 1", function (err, result, fields) {
// 		    if (err){
// 		    	return
// 		    }else{
// 		    	if( result[0].version != null ){
// 		    		pushRow(result[0].version)
// 		    	}else{
// 		    		res.status(200).json({ message: 'error' });
// 		    	}
// 			}
// 		});
// 	}

// 	function pushRow(version){

// 		var v = version+0.0001;

// 		db.query(
// 			"INSERT INTO acces (zone_id, badge_id, date_debut, date_fin ,created_at, version) "+
// 			"VALUES ('"+req.body.zoneId+"', '"+req.body.idBadge+"', '"+req.body.date_debut+"', '"+req.body.date_fin+"', '"+req.body.created_at+
// 		"', '"+v+"')");

// 		io.emit('pushData', 
// 			{
// 				'zone'             : req.body.zone,
// 				'idBadge'          : req.body.idBadge,
// 				'created_at'       : req.body.created_at,
// 				'checking_info'    : req.body.checking_info,
// 				'version'          : v,
// 				'inside'           : req.body.inside,
// 			}
// 		);

// 		res.status(200).json({ message: 'success' });
				
// 	}

// 	checkLastVersion();
	
// });


app.post('/pushNewAccess',function(req,res){	

	function checkLastVersion(callback){

		db.query(
			"SELECT * FROM acces ORDER BY ID DESC LIMIT 1", function (err, result, fields) {
		    
		    if (err){
		    	return 
		    }else{

		    	console.log(result)
		    	if( result[0].version != null ){
		    		pushRow(result[0].version)
		    	}else{
		    		res.status(200).json({ message: 'error' });
		    	}
			}
		});

	}

	function pushRow(version){

		var v = version+0.0001;

		console.log(version+0.0001);

		// for (var i = req.body.length - 1; i >= 0; i--) {
		// 	if( req.body[i]. == null ){
		// 		res.status(200).json({ message: 'error' });
		// 	}
		// }

		db.query(
			"INSERT INTO acces (zone_id, date_debut, date_fin, badge_id, created_at ,version) "+
			"VALUES ('"+req.body.zone_id+"', '"+req.body.date_debut+"' , '"+req.body.date_fin+"' , '"+req.body.badge_id+"', '"+req.body.created_at+"', '"+v+"')");

		db.query( "SELECT * FROM acces LEFT JOIN zones ON acces.zone_id = zones.id LEFT JOIN badges ON acces.badge_id = badges.uid ORDER BY acces.id DESC LIMIT 1" , function (req,res) {
			
			console.log(res);
		    io.emit('pushNewAccess', res);		    

		});

		res.status(200).json({ message: 'success' });

	}

	checkLastVersion();

});

app.get('/getZones',function(req,res){

	db.query(
		"SELECT * FROM zones ", function (err, result, fields) {
	    
	    if (err){
	    	es.status(200).json({ message: 'error'});
	    }else{
	    	res.status(200).json({ message: 'success', 'data' : result, 'count' : result.length });
		}

	});

});

app.post('/setZone', function (req, res) {
	var id = req.params.id;
	console.log(req.body)
	// res.send('Response send to client::'+req.params.id);
	// // res.sendFile(__dirname + '/setZone.html');
	// // res.status(200).json({ message: 'error'});
})


app.get('/getAccess',function(req,res){

	db.query(
		"SELECT * FROM acces LEFT JOIN zones ON acces.zone_id = zones.id LEFT JOIN badges ON acces.badge_id = badges.uid", function (err, result, fields) {
	    
	    if (err){
	    	res.status(200).json({ message: 'error'});
	    }else{
	    	res.status(200).json({ message: 'success', 'data' : result, 'count' : result.length });
		}

	});

});


app.get('/getListBadge',function(req,res){

	db.query(
		"SELECT * FROM badges ORDER BY id", function (err, result, fields) {
	    
	    if (err){
	    	res.status(200).json({ message: 'error'});
	    }else{
	    	res.status(200).json({ message: 'success', 'data' : result, 'count' : result.length });
		}

	});

});

app.get('/getlogs',function(req,res){

	db.query(
		"SELECT * FROM badging "+
		"LEFT JOIN zones ON badging.zone_id = zones.id "+
		"LEFT JOIN badges ON badging.uid_badge = badges.uid "+
		"ORDER BY badging.id DESC ", function (err, result, fields) {
	    
	    if (err){
	    	res.status(200).json({ message: 'error'});
	    }else{
	    	res.status(200).json({ message: 'success', 'data' : result, 'count' : result.length });
		}

	});

});


app.get('/listZones', function(req, res){
  res.sendFile(__dirname + '/listezones.html');
});



app.get('/getAutorisations',function(req,res){

	db.query(
		"SELECT * FROM acces LEFT JOIN zones ON acces.zone_id = zones.id LEFT JOIN badges ON acces.badge_id = badges.id  ", function (err, result, fields) {
	    
	    if (err){
	    	res.status(200).json({ message: err});
	    }else{
	    	res.status(200).json({ message: 'success', 'data' : result});
		}
	});

});

app.post('/getNewAccess',function(req,res){ // Fonction qui ramene les new datas entre deux versions

	function checkLastVersion(version , callback){

		db.query(
			"SELECT * FROM acces ORDER BY ID DESC LIMIT 1", function (err, result, fields) {
		    
		    if (err){
		    	return 
		    }else{
		    	getLastVersion(version, result[0].version)
			}
		});

	}

	function getLastVersion(version, lastVersion){

		console.log(lastVersion)
		console.log(version)

		if( version < lastVersion ){
	    		
			db.query(
				"SELECT * FROM acces LEFT JOIN zones ON acces.zone_id = zones.id LEFT JOIN badges ON acces.badge_id = badges.uid WHERE (acces.version > "+version+" ) ", function (e, r, f) {
					// console.log(r)
				res.status(200).json({ 'message' : 'success', 'datas' : r }); 
			});

		}else{
			res.status(200).json({ 'message' : 'nothing', 'datas' : [] });
		}
				
	}

	var version = req.body.version;
	var lastVersion ;
	var datas;
	console.log('Version demandée : '+version);

	checkLastVersion(version);

});


io.on('connection', function(socket){

	console.log('User connected');
	console.log(socket.id);
	console.log(socket.connected);

	socket.on('chat message', function(msg){
		io.emit('chat message', msg);
	});

	socket.on('newAutorisation', function(data){
		logs.push(data);
		socket.broadcast.emit('newAutorisation', logs);
		console.log('Heure : ' + data.checking_info);
		db.query("INSERT INTO badging (terminal_zone, uid_badge, created_at, checking_infos) VALUES ('"+data.zone+"', '"+data.idBadge+"', '"+data.created_at+"', '"+data.checking_info+"')");
	});

	socket.on('activateBadge', function(data){
		// logs.push(data);
		socket.broadcast.emit('activateBadge', data);
		console.log('Badge : ' + data);
		db.query("UPDATE badges SET status = '1' WHERE badges.uid = '"+data+"'");
		// socket.broadcast.emit('init', data);
	});

	socket.on('desactivateBadge', function(data){
		// logs.push(data);
		socket.broadcast.emit('desactivateBadge', data);
		console.log('Badge : ' + data);
		db.query("UPDATE badges SET status = '0' WHERE badges.uid = '"+data+"'");
	});

	socket.on('init', function(data){
		var datas = [];

		socket.broadcast.emit('init', data);
		db.query("SELECT * FROM badging ORDER BY created_at", function(req,res){
			io.emit('init', res);
			datas['badging'] = res;
			console.log(datas)
		});

	});

	socket.on('pushNewAccess', function(data){

		var datas = [];
		db.query( "SELECT * FROM acces LEFT JOIN zones ON acces.zone_id = zones.id LEFT JOIN badges ON acces.badge_id = badges.uid ORDER BY acces.id DESC LIMIT 1;" , function (req,res) {
		    
		    console.log(res)
		    io.emit('pushNewAccess', res);

		});

	});

	socket.on('pushData', function(data){
		var datas = [];
		db.query("SELECT * FROM badging LEFT JOIN zones ON badging.zone_id = zones.id ORDER BY badging.id DESC  ", function(req,res){
			io.emit('getLogs', res);
		});

	});

	socket.on('getListBadge', function(data){

		var datas = [];
		db.query( "SELECT * FROM badges ORDER BY id DESC" , function (req,res) {
		    
		    io.emit('getListBadge', res);

		});

	});

	socket.on('getZones', function(data){
		db.query(
			"SELECT * FROM zones ", function (req, res) {
		    io.emit('getZones', res);
		});
	});


});


http.listen(3000, function(){
  console.log('listening on *:3000');
});





















































