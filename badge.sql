-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  lun. 21 oct. 2019 à 02:13
-- Version du serveur :  8.0.16
-- Version de PHP :  7.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `badge`
--

-- --------------------------------------------------------

--
-- Structure de la table `acces`
--

CREATE TABLE `acces` (
  `id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `badge_id` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `version` decimal(10,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `acces`
--

INSERT INTO `acces` (`id`, `zone_id`, `date_debut`, `date_fin`, `badge_id`, `created_at`, `version`) VALUES
(65, 3, '2019-10-20 17:42:04', '2019-10-25 19:42:00', '6559E915', '2019-10-20 17:42:04', '1.0000'),
(85, 1, '2019-10-20 17:42:04', '2019-10-25 19:42:00', '65215315', '2019-10-20 17:42:04', '1.0001');

-- --------------------------------------------------------

--
-- Structure de la table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
--
-- Déchargement des données de la table `admins`
--

INSERT INTO `admins` (`id`, `email`, `password`, `created_at`) VALUES
(1, 'user@gmail.com', 'azerty', '2019-10-20 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `badges`
--

CREATE TABLE `badges` (
  `id` int(11) NOT NULL,
  `uid` varchar(200) CHARACTER SET utf8mb4 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nom_sexy` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Déchargement des données de la table `badges`
--

INSERT INTO `badges` (`id`, `uid`, `status`, `nom_sexy`) VALUES
(1, '6559E915', 1, 'Alassane'),
(2, '65215315', 1, 'Gon'),
(3, '65179215', 1, 'Duncan');

-- --------------------------------------------------------

--
-- Structure de la table `badging`
--

CREATE TABLE `badging` (
  `id` int(11) NOT NULL,
  `zone_id` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `checking_infos` enum('ok','nonok','ko','') NOT NULL,
  `uid_badge` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `created_at` datetime NOT NULL,
  `version` decimal(10,4) NOT NULL,
  `inside` varchar(100) NOT NULL DEFAULT 'false'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Déchargement des données de la table `badging`
--

INSERT INTO `badging` (`id`, `zone_id`, `checking_infos`, `uid_badge`, `created_at`, `version`, `inside`) VALUES
(13, '3', 'ok', '6559E915', '2019-10-21 01:13:09', '1.0000', 'false'),
(70, '3', 'ok', '6559E915', '2019-10-21 02:10:53', '1.0001', 'true'),
(71, '3', 'ok', '6559E915', '2019-10-21 02:11:02', '1.0002', 'false'),
(72, '3', 'ok', '6559E915', '2019-10-21 02:11:07', '1.0003', 'true'),
(73, '1', 'ok', '6559E915', '2019-10-21 02:11:32', '1.0004', 'false'),
(74, '1', 'ok', '65215315', '2019-10-21 02:12:28', '1.0005', 'true'),
(75, '1', 'ok', '65215315', '2019-10-21 02:12:34', '1.0006', 'true'),
(76, '1', 'ok', '65215315', '2019-10-21 02:12:39', '1.0007', 'false'),
(77, '1', 'ok', '65215315', '2019-10-21 02:12:45', '1.0008', 'true'),
(78, '1', 'ok', '65215315', '2019-10-21 02:12:49', '1.0009', 'false');

-- --------------------------------------------------------

--
-- Structure de la table `zones`
--

CREATE TABLE `zones` (
  `id` int(11) NOT NULL,
  `libelle` varchar(200) NOT NULL,
  `id_parent_fk` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Déchargement des données de la table `zones`
--

INSERT INTO `zones` (`id`, `libelle`, `id_parent_fk`) VALUES
(1, 'PRESIDENCE', 1),
(2, 'ENTREE POSTE', 1),
(3, 'ENTREE CASH', 1),
(4, 'PALAIS', 1),
(5, 'PETIT PALAIS', 1),
(6, 'CONSEILLERS', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `acces`
--
ALTER TABLE `acces`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `badges`
--
ALTER TABLE `badges`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `badging`
--
ALTER TABLE `badging`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `acces`
--
ALTER TABLE `acces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT pour la table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `badges`
--
ALTER TABLE `badges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `badging`
--
ALTER TABLE `badging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT pour la table `zones`
--
ALTER TABLE `zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
